#!/usr/bin/env ruby
# coding: utf-8

require "fileutils"
require "optparse"

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: #{$0} [options]"
      "\nInstalls André Offringa's software."
    opts.on("-h", "--help", "Display this message.") do
        puts opts
        exit
    end
end.parse!

raise "Must run as root" unless Process.uid.zero?

def psystem(command)
    puts "+ #{command}"
    system("sh -c \"#{command}\"")
end

def cmake(dir)
    Dir.chdir dir
    psystem("git pull")
    FileUtils.rm_r "build" if File.exist?("build")
    FileUtils.mkdir_p "build"
    Dir.chdir "build"

    # Hack for python3 crap. Seriously, how much damage has this transition
    # done? Can we *please* stop using garbage?
    if dir == "cotter"
        psystem("sed -i 's/\\(find_package(Boost.* \\)python3 numpy3\\(.*\\)/\\1python numpy\\2/' ../CMakeLists.txt")
    end

    psystem("cmake ..")
    psystem("make -j8")
    psystem("make install")
    Dir.chdir("../..")
end

base = Dir.pwd

psystem("apt install -y aoflagger \
                        aoflagger-dev \
                        wsclean \
                        wsclean-dev \
                        cmake \
                        gcc \
                        g++ \
                        gfortran \
                        libopenblas-dev \
                        libboost-program-options-dev \
                        libboost-dev \
                        libboost-date-time-dev \
                        libboost-system-dev \
                        libboost-filesystem-dev \
                        libboost-thread-dev \
                        libboost-python-dev \
                        libboost-numpy-dev \
                        libgsl-dev \
                        libxml++2.6-dev \
                        libcfitsio-dev \
                        libfftw3-dev \
                        libpng-dev \
                        libgtkmm-3.0-dev \
                        liberfa-dev")

# Download a fresh copy of the measurements, if none already exist or they are
# too old.
measures = Dir.glob("WSRT_Measures*.ztar")
unless measures.empty?
    latest = measures.max_by { |l| File.mtime(l) }
    time_diff = Time.now - File.mtime(latest)
    if time_diff > (24 * 3600)
        FileUtils.rm latest
        download = true
    end
else
    download = true
end

if download
    psystem("wget ftp://ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar -O WSRT_Measures.ztar")
    FileUtils.rm_r "/var/lib/casacore/data" if File.exist? "/var/lib/casacore/data"
    FileUtils.mkdir_p "/var/lib/casacore/data"
    Dir.chdir "/var/lib/casacore/data"
    psystem("tar -xf #{base}/WSRT_Measures.ztar")
    Dir.chdir base
end

unless File.exist?("pal-0.9.8.tar.gz")
    psystem("wget https://github.com/Starlink/pal/releases/download/v0.9.8/pal-0.9.8.tar.gz")
    psystem("tar -xf pal-0.9.8.tar.gz")
end
Dir.chdir "pal-0.9.8"
psystem("./configure --prefix=/usr/local --without-starlink")
psystem("make -j8")
psystem("make install")
Dir.chdir ".."

psystem("git clone https://github.com/aroffringa/dysco.git") unless File.exist?("dysco")
cmake "dysco"

psystem("git clone https://github.com/MWATelescope/cotter.git") unless File.exist?("cotter")
cmake "cotter"

unless File.exist? "mwa-reduce"
    puts "Can't install calibrate, because 'mwa-reduce' is not here. Clone that from https://github.com/ICRAR/mwa-reduce"
else
    Dir.chdir "mwa-reduce"
    FileUtils.rm_r "build" if File.exist?("build")
    FileUtils.mkdir_p "build"
    Dir.chdir "build"
    psystem("cmake ..")
    psystem("make -j8")
    psystem("find . -maxdepth 1 -type f -executable -exec cp {} /usr/local/bin \\;")
end
