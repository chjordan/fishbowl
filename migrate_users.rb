#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

# ARGV[0] points to the old root partition. We assume folders "etc" and "home" sit there.
raise "Please specify where the old root partition is." if ARGV.empty?
raise "Please specify the users to be migrated." if ARGV.length == 1

old_root = ARGV[0]
users = ARGV[1..-1]

old_etc_passwd = File.read("#{old_root}/etc/passwd")
old_etc_shadow = File.read("#{old_root}/etc/shadow")

# For every user...
users.each do |u|
    puts u
    if File.exist?("/home/#{u}")
        puts "User #{u} already exists on the new system! Skipping..."
        next
    end

    # Retrieve the old "comment" from the old /etc/passwd
    comment = old_etc_passwd.scan(/^#{u}:x:\d+:\d+:(.*):\S+:/).first.first

    # Retrieve the old password from the old /etc/shadow
    maybe_password = old_etc_shadow.scan(/^(#{u}\S+)/).first
    if maybe_password.nil?
        puts "User #{u} doesn't have a password on the old root; skipping..."
        next
    end
    password = maybe_password.first.split(":")[1]
    puts "WARNING: Password is empty! Login is possible without a password!" if password.empty?

    # Add the new user with useradd.
    system("useradd -m -c \"#{comment}\" -s /bin/bash -p \"#{password}\" #{u}")

    # Copy the old home directory to the new one.
    system("rsync -a --chown=#{u}:#{u} #{old_root}/home/#{u} /home/")
end
