#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

def psystem(command)
    puts command
    system("sh -c \"#{command}\"")
end

psystem("apt update")
psystem("apt install -y r-base r-base-dev libjpeg62-dev")

remote_file = "https://download1.rstudio.org/rstudio-xenial-1.1.456-amd64.deb"
stem = File.basename(remote_file).split("/").last
system("wget #{remote_file}") unless File.exist?(File.basename(remote_file))

psystem("sudo dpkg -i #{stem}")
