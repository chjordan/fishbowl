#!/usr/bin/env ruby

require "fileutils"

raise "Must run as root" unless Process.uid.zero?

pwd = Dir.pwd

file = "sigproc-4.3.tar.gz"
raise "Navigate to https://sourceforge.net/projects/sigproc/files/sigproc/4.3/ and download the tarball." \
    if Dir.glob(file).empty?

Dir.chdir("/opt")
system("tar -xf #{pwd}/#{file}") unless File.exist?("sigproc-4.3")

Dir.chdir("/opt/sigproc-4.3")
# Why do astronomers insist on making crappy, non-standard things?
# Thanks to Bradley Meyers:
# http://www.ljtwebdevelopment.com/pulsarref/pulsar-software-install-ubuntu-64bit.html
system("./configure << EOF
/opt/sigproc-4.3/bin
EOF")

contents = File.read("/opt/sigproc-4.3/makefile.linux")
contents.gsub!(/(CCC = .*)/, "CCC = gcc -O2 -fPIC")
contents.gsub!(/(#LPGPLOT = .*)/, "LPGPLOT = -lpgplot -lcpgplot -L/usr/X11R6/lib -lX11 -lpng")
contents.gsub!(/(#LFITS = .*)/, "LFITS = -lcfitsio")
contents.gsub!(/(#LFFTW = .*)/, "LFFTW = -lfftw3 -lfftw3f")
contents += "FC = gfortran -ffixed-line-length-none -fPIC\n"
File.open("/opt/sigproc-4.3/makefile.linux", "w") { |f| f.puts contents }

# Specifying OSTYPE=linux doesn't seem to work.
contents = File.read("/opt/sigproc-4.3/makefile")
File.open("/opt/sigproc-4.3/makefile", "w") { |f| f.puts contents.gsub(/\$\(OSTYPE\)/, "linux") }

# Compile errors - nice.
# gfortran   -c -o dosearch.o dosearch.f
# dosearch.f:265.28:

#          write(llog,*) 'DB\'s slow-but-simple harmonic summing routine'
#                             1
# Error: Syntax error in WRITE statement at (1)
# make: *** [dosearch.o] Error 1
contents = File.readlines("/opt/sigproc-4.3/dosearch.f")
contents[264].gsub!(/DB\\'s/, "DBs")
File.open("/opt/sigproc-4.3/dosearch.f", "w") { |f| f.puts contents }

# Newer versions of make choke? Not sure how to fix this properly, but the export rule doesn't
# appear to be used.
contents = File.readlines("/opt/sigproc-4.3/makefile")
contents[451].gsub!(/(.*)\n/, "#\\1")
contents[452].gsub!(/(.*)\n/, "#\\1")
File.open("/opt/sigproc-4.3/makefile", "w") { |f| f.puts contents }

FileUtils.mkdir_p("/opt/sigproc-4.3/bin")
system("make")
system("make quickplot")

env = File.read("/etc/environment")
unless env.include?("sigproc-4.3")
    File.open("/etc/environment", "w") \
    { |f| f.puts env.sub(/(PATH=.*)"/, "\\1:/opt/sigproc-4.3/bin\"") }
end
