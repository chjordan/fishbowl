#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

pwd = Dir.pwd

# Download the tarball, if it's not in the PWD.
remote_file = "http://heasarc.gsfc.nasa.gov/FTP/software/lheasoft/release/heasoft-6.24src.tar.gz"
system("wget #{remote_file}") unless File.exist?(File.basename(remote_file))

unpacked_dir = File.basename(remote_file).split("src").first
system("tar -xf #{File.basename(remote_file)}") unless File.exist?(unpacked_dir)
Dir.chdir("#{unpacked_dir}/BUILD_DIR")

# This takes an absurdly long time (~1h).
system("./configure --prefix=/opt/heasoft")
system("make -j1")
system("make install")

# HEAsoft does awful things to environment variables. Use another script to
# make wrapper scripts (have I gone completely crazy?)
system("#{pwd}/heasoft_wrappers.sh")

# Add the wrappers directory to the environment.
env = File.read("/etc/environment")
unless env.include?("heasoft")
    env.sub!(/(PATH=.*)"/, "\\1:/opt/heasoft-wrappers\"")
    File.open("/etc/environment", "w") { |f| f.puts env }
end
