#!/usr/bin/env bash

# HEAsoft (https://heasarc.gsfc.nasa.gov/docs/software/lheasoft/) does
# unspeakable things with environment variables, which happily trample
# anything else that might also use said environment variables. This script
# will wrap each executable from HEAsoft with a script that correctly calls
# the executable.


if [[ ! $EUID -eq 0 ]]; then
    echo "Must run as root"
    exit 1
fi

# At the time of writing, the glibc version could be 2.26 (Ubuntu 17.10) or 2.27 (Ubuntu 18.04).
# Match with ?.??, and just use the first version found.
heasoft_versions=(/opt/heasoft/x86_64-pc-linux-gnu-libc?.??)
heasoft_root="${heasoft_versions[0]}"
heasoft_wrappers="/opt/heasoft-wrappers"

mkdir -p $heasoft_wrappers

for b in "${heasoft_root[@]}"/bin/*; do
    base=$(basename "$b")

    if [[ -d $base ]]; then
        ln -s "$b" .
    else
        echo "#!/usr/bin/env ruby
args = []
ARGV.each do |a|
    p, s = a.split('=')
    if s
        args.push(\"#{p}='#{s}'\")
    else
        args.push(\"#{p}\")
    end
end
exec \"LD_LIBRARY_PATH=${heasoft_root}/lib:\$LD_LIBRARY_PATH \\
     HEADAS=${heasoft_root} \\
     LHEASOFT=${heasoft_root} \\
     FTOOLS=${heasoft_root} \\
     LHEAPERL=/usr/bin/perl \\
     EXT=lnx \\
     PERLLIB=${heasoft_root}/lib/perl \\
     PERL5LIB=${heasoft_root}/lib/perl \\
     PFCLOBBER=1 \\
     PFILES=${heasoft_root}/syspfiles \\
     FTOOLSINPUT=stdin \\
     FTOOLSOUTPUT=stdout \\
     LHEA_DATA=${heasoft_root}/refdata \\
     LHEA_HELP=${heasoft_root}/help \\
     PGPLOT_RGB=${heasoft_root}/lib/rgb.txt \\
     PGPLOT_DIR=${heasoft_root}/lib \\
     POW_LIBRARY=${heasoft_root}/lib/pow \\
     PYTHONPATH=${heasoft_root}/lib/python:${heasoft_root}/lib \\
     LYNX_CFG=${heasoft_root}/lib \\
     XRDEFAULTS=${heasoft_root}/xrdefaults \\
     TCLRL_LIBDIR=${heasoft_root}/lib \\
     XANADU=/opt/heasoft \\
     XANBIN=${heasoft_root} \\
     ${heasoft_root}/bin/${base} #{args.join(' ')}\"" > "$heasoft_wrappers/$base"
        chmod +x "$heasoft_wrappers/$base"
    fi
done
