#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

def psystem(command)
    puts command
    system("sh -c \"#{command}\"")
end

# https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository
psystem("apt update")
psystem("apt install -y apt-transport-https ca-certificates curl software-properties-common")
psystem("curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -")
psystem("add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu #{`lsb_release -cs`} stable'")
psystem("apt update")
psystem("apt install -y docker-ce")
psystem("docker run hello-world")
