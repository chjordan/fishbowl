#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

pwd = Dir.pwd

# Download the tarball, if it's not in the PWD.
remote_file = "https://alma-dl.mtk.nao.ac.jp/ftp/casa/distro/linux/release/el7/casa-release-5.6.0-60.el7.tar.gz"
stem = File.basename(remote_file).split(".")[0..-3].join(".")
system("wget #{remote_file}") unless File.exist?(File.basename(remote_file))

Dir.chdir("/opt")
system("tar -xf #{pwd}/#{File.basename(remote_file)}") unless File.exist?(stem)

env = File.read("/etc/environment")
unless env.include?(stem)
    File.open("/etc/environment", "w") \
    { |f| f.puts env.sub(/(PATH=.*)"/, "\\1:/opt/#{stem}/bin\"") }
end
